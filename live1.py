from flask import Flask, render_template,send_file
from flask_socketio import SocketIO, emit
import pymongo
import openpyxl
from datetime import datetime
import os
from bson.objectid import ObjectId
#------------------------------
# LOCAL DATABASE INITIALIZE
#------------------------------
myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["kpcl"]
joblist_db = mydb["testbed1"]
joblist_db.drop()
#---------------------
#Queries
#---------------------
def return_job_serial_nos():
    serial_number =[]
    print("------------------------------")
    for x in joblist_db.find({}, {"_id": 1}):
        serial_number.append(str(x["_id"]))
        print(x["_id"])
    print("------------------------------")
    serial_dict ={"serial_nos":serial_number}
    return  serial_dict
def return_job_details(serial_id):
    myquery = {"_id": ObjectId(serial_id)}

    mydoc = joblist_db.find(myquery)
    for x in mydoc:
        print(x)
    return  x
def return_job_details_jobtype(jobtype):
    myquery = {"jobtype": jobtype}

    mydoc = joblist_db.find(myquery).sort("_id",pymongo.ASCENDING)
    all_x=[]
    for x in mydoc:
        #print(x)
        x["_id"]=str(x["_id"])
        all_x.append(x)
    return  all_x
#----------------------------
from pymodbus.client.sync import ModbusSerialClient as Modbusclient
import time
import math
from ctypes import *
'''import RPi.GPIO as GPIO
import time
import time
import Adafruit_ADS1x15'''

from pymodbus.constants import Endian
from pymodbus.payload import BinaryPayloadDecoder
from pymodbus.payload import BinaryPayloadBuilder







from bson import ObjectId



def live1_thread(msg,socketio,session_details):
    print("----------------------------IN LIVE1 THREAD------------------------------")
    print(msg)
    time.sleep(10)
    socketio.emit('message',{'Success':"Mine"})



    nozzle_constant = 73.39

    test_time = 500
    reading1_time = 100
    reading2_time = 250
    reading3_time = 450


    # Global Variables
    col = 'C'
    comp_air_oil_temp = 100
    comp_air_oil_pressure = 101
    dpt1 = 450
    dpt2 = 1000
    voltage = 28
    current = 50
    power = 300
    temp_out = 30
    # ---------------------------
    loading_pressure = 13
    unloading_pressure = 12
    safety_valve_pressure = 14.5

    book = openpyxl.load_workbook("R2_parameters.xlsx")
    sheet = book["Internal"]
    now = datetime.now()
    current_time = now.strftime("%H:%M")
    sheet['F7'] = current_time
    book.save('R2_parameters.xlsx')
    book.close()

    #os.write(libreoffice - -headless - -convert - to pdf *.xlsx)

    def convert(s):
        i = int(s, 16)  # convert from hex to a Python int
        cp = pointer(c_int(i))  # make this into a c integer
        fp = cast(cp, POINTER(c_float))  # cast the int pointer to a float pointer
        return fp.contents.value  # dereference the pointer, get the float

    # Compressor Air Oil Temp : 40504
    # Compressor Oil Pressure : 40496
    # Unload Pressure :40480
    # Load Pressure : 40481

    '''GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)

    dpt1_ad = 17
    dpt2_ad = 27
    pt100_ad = 22

    GPIO.setup(dpt1_ad, GPIO.OUT)
    GPIO.setup(dpt2_ad, GPIO.OUT)
    GPIO.setup(pt100_ad, GPIO.OUT)

    GPIO.setup(10, GPIO.OUT)
    GPIO.setup(9, GPIO.OUT)
    GPIO.setup(11, GPIO.OUT)
    GPIO.setup(5, GPIO.OUT)
    GPIO.setup(6, GPIO.OUT)

    GPIO.setup(dpt1_ad, GPIO.OUT)
    GPIO.setup(dpt2_ad, GPIO.OUT)
    GPIO.setup(pt100_ad, GPIO.OUT)

    GPIO.setup(10, GPIO.HIGH)
    GPIO.setup(9, GPIO.HIGH)
    GPIO.setup(11, GPIO.HIGH)
    GPIO.setup(5, GPIO.HIGH)
    GPIO.setup(6, GPIO.HIGH)

    adc = Adafruit_ADS1x15.ADS1115()
    GAIN = 2 / 3'''

    # measure dpt1
    # GPIO.output(dpt1_ad,GPIO.HIGH)
    # GPIO.output(dpt2_ad,GPIO.LOW)
    # GPIO.output(pt100_ad,GPIO.LOW)

    # measure dpt2
    # GPIO.output(dpt1_ad,GPIO.LOW)
    # GPIO.output(dpt2_ad,GPIO.HIGH)
    # GPIO.output(pt100_ad,GPIO.LOW)

    # measure pt100
    # GPIO.output(dpt1_ad,GPIO.LOW)
    # GPIO.output(dpt2_ad,GPIO.LOW)
    # GPIO.output(pt100_ad,GPIO.HIGH)

    '''GPIO.output(dpt1_ad, GPIO.LOW)
    GPIO.output(dpt2_ad, GPIO.LOW)
    GPIO.output(pt100_ad, GPIO.LOW)'''

    time.sleep(5)

    temp = [0] * 8

    # client = system microcontroller
    # client1 = mfm

    '''client = Modbusclient(method='rtu', port='/dev/ttyUSB0', stopbits=1, bytesize=8, parity='N', baudrate=9600,
                          timeout=0.3)

    connection = client.connect()
    print(connection)

    client1 = Modbusclient(method='rtu', port='/dev/ttyUSB1', stopbits=1, bytesize=8, parity='E', baudrate=19200,
                           timeout=0.3)

    connection = client1.connect()
    print(connection)'''

    '''sid = client.write_register( 456,7, unit=10)
    time.sleep(2)
    print(sid)
    value = client.read_holding_registers(455,12,unit=10)
    #print('[{}]'.format(', '.join(hex(x) for x in value.registers)))
    print(value.registers)
    #sid=client.write_register(480, 0b0000000000001111)
    #print(sid)'''

    def take_readings():
        var1 = 500
        result = [100, 100]

        '''value = client.read_holding_registers(503, 2, unit=10)
        # print('[{}]'.format(', '.join(hex(x) for x in value.registers)))

        result = value.registers
        var = str(hex(result[1])) + str(hex(result[0]))
        var = var.replace("0x", "")
        # print(var)
        var = convert(var)
        var1 = round(float(var) * 1, 3)'''

        print("AOS Tank Temp : " + str(var1))
        comp_air_oil_temp = var1

        socketio.emit('message', {"AOS Tank Temp": str(var1)})

        time.sleep(1)

        '''value = client.read_holding_registers(495, 2, unit=10)

        # print('[{}]'.format(', '.join(hex(x) for x in value.registers)))

        result = value.registers
        var = str(hex(result[1])) + str(hex(result[0]))
        var = var.replace("0x", "")
        # print(type(var))
        var = convert(var)
        var1 = round(float(var), 3)'''

        print("AOS Tank Pressure : " + str(var1))
        socketio.emit('message', {"AOS Tank Pressure": str(var1)})
        comp_air_oil_pressure = var1


        time.sleep(3)

        print("Air Inlet Temp : " + str(32))
        socketio.emit('message', {"Air Inlet Temp": str(32)})

        '''value = client.read_holding_registers(497, 2, unit=10)
        # print('[{}]'.format(', '.join(hex(x) for x in value.registers)))

        result = value.registers
        var = str(hex(result[1])) + str(hex(result[0]))
        var = var.replace("0x", "")
        # print(type(var))
        var = convert(var)
        var1 = round(float(var), 3)
        discharge_press = var1'''
        print("Discharge Pressure : " + str(var1))
        socketio.emit('message', {"Discharge Pressure": str(var1)})

        time.sleep(1)

        '''value = client.read_holding_registers(478, 1, unit=10)
        # print('[{}]'.format(', '.join(hex(x) for x in value.registers)))

        result = value.registers
        # var=hex(result[0])
        # var=var.replace("0x","")
        # print(type(var))
        # var = convert(var)
        var1 = round(var, 3)

        print("Diffrential Pressure : " + str(result[0] / 10))'''

        socketio.emit('message', {"Differential pressure": str(result[0] / 10)})

        time.sleep(1)

        '''value = client.read_holding_registers(479, 1, unit=10)
        # print('[{}]'.format(', '.join(hex(x) for x in value.registers)))

        result = value.registers
        # var=hex(result[0])
        # var=var.replace("0x","")
        # print(type(var))
        # var = convert(var)
        var1 = round(var, 3)'''

        print("Unload Pressure : " + str(result[0] / 10))
        socketio.emit('message', {"Unload Pressure": str(result[0] / 10)})

        time.sleep(1)

        print("----------------------------------\n")

        '''value = client1.read_holding_registers(3908, 2, unit=0x01)
        # print('[{}]'.format(', '.join(hex(x) for x in value.registers)))

        result = value.registers
        var = str(hex(result[1])) + str(hex(result[0]))
        var = var.replace("0x", "")
        # print(var)
        var = convert(var)
        var1 = round(float(var) * 1, 3)'''

        print("Voltage :" + str(var1))
        socketio.emit('message', {"Voltage": str(var1)})
        voltage = var1

        time.sleep(1)

        '''value = client1.read_holding_registers(3902, 2, unit=0x01)
        # print('[{}]'.format(', '.join(hex(x) for x in value.registers)))

        result = value.registers
        var = str(hex(result[1])) + str(hex(result[0]))
        var = var.replace("0x", "")
        # print(var)
        var = convert(var)
        var1 = round(float(var) * 1, 3)'''

        print("Power :" + str(var1))
        socketio.emit('message', {"Power": str(var1)})
        power = var1
        time.sleep(1)

        '''value = client1.read_holding_registers(3912, 2, unit=0x01)
        # print('[{}]'.format(', '.join(hex(x) for x in value.registers)))

        result = value.registers
        var = str(hex(result[1])) + str(hex(result[0]))
        var = var.replace("0x", "")
        # print(var)
        var = convert(var)
        var1 = round(float(var) * 1, 3)'''

        print("Current :" + str(var1))
        socketio.emit('message', {"Current": str(var1)})
        current = var1
        print("----------------------------------\n")

        temp = [0] * 8
        dpt500 = 200
        dpt1000 = 1000
        temp1 = 50
        '''# measure dpt1
        GPIO.output(dpt1_ad, GPIO.HIGH)
        GPIO.output(dpt2_ad, GPIO.LOW)
        GPIO.output(pt100_ad, GPIO.LOW)

        time.sleep(1)
        for i in range(0, 8):
            temp[i] = ((adc.read_adc(2, gain=GAIN) / 32767 * 6.144) - 1.02) * 125
        dpt500 = sum(temp) / 8'''
        print("DPT1 :" + str(dpt500))
        socketio.emit('message', {"DPT1": str(dpt500)})
        dpt1 = dpt500
        time.sleep(1)
        temp = [0] * 8
        # measure dpt2
        '''GPIO.output(dpt1_ad, GPIO.LOW)
        GPIO.output(dpt2_ad, GPIO.HIGH)
        GPIO.output(pt100_ad, GPIO.LOW)

        time.sleep(1)
        for i in range(0, 8):
            temp[i] = ((adc.read_adc(2, gain=GAIN) / 32767 * 6.144) - 1.01) * 250
        dpt1000 = sum(temp) / 8'''
        print("DPT2 :" + str(dpt1000))
        socketio.emit('message', {"DPT2": str(dpt1000)})
        dpt2 = dpt1000

        time.sleep(1)
        temp = [0] * 8
        # measure pt100
        '''GPIO.output(dpt1_ad, GPIO.LOW)
        GPIO.output(dpt2_ad, GPIO.LOW)
        GPIO.output(pt100_ad, GPIO.HIGH)

        time.sleep(1)
        for i in range(0, 8):
            temp[i] = ((adc.read_adc(2, gain=GAIN) / 32767 * 6.144) - 2.3) / 0.006975'''
        temp1 = sum(temp) / 8
        print("TEMP :" + str(temp1))
        socketio.emit('message', {"TEMP": str(temp1)})
        temp_out=temp1
        time.sleep(1)

        print("-----------------------------\n")
        FAD = 5
        '''ambient_temp = 29
        ambient_pressure = 720
        ambient_humidity = 65
        suction_filter_drop = 0

        upstream_press = dpt500
        downstream_press = dpt1000
        air_outlet_temp = temp1
        air_compressor_delivery_pressure_outlet = 6.6

        sid = (upstream_press + ambient_pressure * 13.52) * downstream_press / (273 + air_outlet_temp)

        print("sid " + str(sid))
        FAD = "NA"
        if sid > 0:
            FAD = nozzle_constant * ((ambient_temp + 273) / (ambient_pressure * 13.52 - suction_filter_drop)) * \
                  (math.sqrt(sid)) / 60'''

        print("FAD :" + str(FAD))
        socketio.emit('message', {"FAD": str(FAD)})

        print("-----------------------------\n")
        time.sleep(2)
        condensate = 0.08
        print("Condensate :" + str(condensate))
        socketio.emit('message', {"condensate": str(condensate)})








    #-----------------------------------------------------------------------------------------------------------------

    # enable write
    socketio.emit('message', {"Message": "Set Unload to 12.5"})


    '''sid = client.write_register(461, 0b1000000000000000, unit=10)
    time.sleep(2)
    print(sid)

    # set unload to 7,5
    sid1 = client.write_register(479, unloading_pressure, unit=10)
    time.sleep(2)
    print(sid1)'''

    # turn on
    socketio.emit('message', {"Message": "Turning on"})

    '''sid = client.write_register(461, 0b1010000000000000, unit=10)
    time.sleep(2)
    print(sid)'''

    # wait for discharge pressure to be greater than 7,0
    socketio.emit('message', {"Message": "Waiting for discharge presure to be greater than Unload Pressure"})

    var1 = 0

    '''while var1 < unload_pressure:
        value = client.read_holding_registers(497, 2, unit=10)
        # print('[{}]'.format(', '.join(hex(x) for x in value.registers)))

        result = value.registers
        var = str(hex(result[1])) + str(hex(result[0]))
        var = var.replace("0x", "")
        # print(type(var))
        var = convert(var)
        var1 = round(float(var), 3)

        print("Discharge Pressure : " + str(var1))
        socketio.emit('message', {"Discharge Pressure": str(var1)})

        time.sleep(1)'''

    def generate_report(col,comp_air_oil_temp , comp_air_oil_pressure , dpt2 ,dpt1,voltage,current,power,temp_out,loading_pressure,unloading_pressure,safety_valve_pressure):
        book = openpyxl.load_workbook("R2_parameters.xlsx")
        sheet = book["Internal"]
        sheet['A10'] = "Auto-Generated Report"
        now = datetime.now()
        current_time = now.strftime("%H:%M")
        print("Current Time =", current_time)
        sheet[col + '12'] = current_time
        sheet[col + '16'] = comp_air_oil_temp
        sheet[col + '17'] = comp_air_oil_pressure
        sheet[col + '25'] = dpt2
        sheet[col + '26'] = dpt1
        sheet[col + '32'] = voltage
        sheet[col + '33'] = current
        sheet[col + '34'] = power
        sheet[col + '24'] = temp_out
        # constants
        sheet[col + '38'] = loading_pressure
        sheet[col + '39'] = unloading_pressure
        # constants
        sheet['C37'] = safety_valve_pressure

        book.save('R2_parameters.xlsx')
        book.close()

    # begin readings
    start_time = time.time()
    while time.time() - start_time < test_time :
        take_readings()
        time_past = time.time() - start_time
        if ( time_past < reading1_time):
            col='C'
            generate_report(col,comp_air_oil_temp , comp_air_oil_pressure , dpt2 ,dpt1,voltage,current,power,temp_out,loading_pressure,unloading_pressure,safety_valve_pressure)
            print("Successfully updated values")
        if (time_past > reading1_time and time_past < reading2_time ):
            col = 'D'
            generate_report(col,comp_air_oil_temp, comp_air_oil_pressure, dpt2, dpt1, voltage, current, power, temp_out,loading_pressure,unloading_pressure,safety_valve_pressure)
            print("Successfully updated values")
        if (time_past > reading2_time and time_past < reading3_time):
            col = 'E'
            generate_report(col,comp_air_oil_temp, comp_air_oil_pressure, dpt2, dpt1, voltage, current, power, temp_out,loading_pressure,unloading_pressure,safety_valve_pressure)
            print("Successfully updated values")

    print("Unloading Machine")
    socketio.emit('message', {"Message": "Unloading Machine"})

    '''# unload
    sid = client.write_register(461, 0b1110000000000000, unit=10)
    time.sleep(2)
    print(sid)'''

    var1 = 8.0
    # wait till discharge pressure is 0
    socketio.emit('message', {"Message": "Wait till discharge pressure is 0."})

    '''while var1 > 0.5:
        value = client.read_holding_registers(497, 2, unit=10)
        # print('[{}]'.format(', '.join(hex(x) for x in value.registers)))

        result = value.registers
        var = str(hex(result[1])) + str(hex(result[0]))
        var = var.replace("0x", "")
        # print(type(var))
        var = convert(var)
        var1 = round(float(var), 3)

        print("Discharge Pressure : " + str(var1))
        socketio.emit('message', {"Discharge Pressure": str(var1)})

        time.sleep(1)'''

    print("No Load Reading")
    socketio.emit('message', {"Message": "Taking No Load Reading"})

    # take power reading --no load power
    take_readings()
    #update no load readings
    book = openpyxl.load_workbook("R2_parameters.xlsx")
    sheet = book["Internal"]
    sheet['C43'] = power
    sheet['E43'] = current
    sheet['G43'] = voltage
    book.save('R2_parameters.xlsx')
    book.close()

    print("Set Unload Pressure to greater than sfp")
    socketio.emit('message', {"Message": "Set Unload Pressure to 14.5"})

    # set unload to 7,5
    '''sid1 = client.write_register(479, 14.7, unit=10)
    time.sleep(2)
    print(sid1)'''

    print("Loading Machine")
    socketio.emit('message', {"Message": "Wait till discharge pressure is 0."})

    # unload
    '''sid = client.write_register(461, 0b1010000000000000, unit=10)
    time.sleep(2)
    print(sid)'''

    # wait till discharge pressure is 0
    '''while var1 < 14.4:
        value = client.read_holding_registers(497, 2, unit=10)
        # print('[{}]'.format(', '.join(hex(x) for x in value.registers)))

        result = value.registers
        var = str(hex(result[1])) + str(hex(result[0]))
        var = var.replace("0x", "")
        # print(type(var))
        var = convert(var)
        var1 = round(float(var), 3)

        print("Discharge Pressure : " + str(var1))
        socketio.emit('message', {"Discharge Pressure": str(var1)})

        time.sleep(1)'''

    print("Started Safety valve Test")
    socketio.emit('message', {"Message": "Started Safety Valve Test"})

    '''counter = 0
    # Safety Valve test begin
    while counter < 2:
        value = client.read_holding_registers(497, 2, unit=10)
        # print('[{}]'.format(', '.join(hex(x) for x in value.registers)))

        result = value.registers
        var = str(hex(result[1])) + str(hex(result[0]))
        var = var.replace("0x", "")
        # print(type(var))
        var = convert(var)
        var1 = round(float(var), 3)

        print("Discharge Pressure : " + str(var1))
        socketio.emit('message', {"Discharge Pressure": str(var1)})

        if var1 < 14:
            counter = counter + 1
            print("Count Occured")
            time.sleep(2)

        if var1 > 14.9:
            sid = client.write_register(461, 0b0000000000000000, unit=10)
            time.sleep(2)
            print(sid)

        time.sleep(0.5)'''

    '''sid = client.write_register(461, 0b0000000000000000, unit=10)
    time.sleep(2)'''

    book = openpyxl.load_workbook("R2_parameters.xlsx")
    sheet = book["Internal"]
    now = datetime.now()
    current_time = now.strftime("%H:%M")
    sheet['F8'] = current_time
    book.save('R2_parameters.xlsx')
    book.close()


