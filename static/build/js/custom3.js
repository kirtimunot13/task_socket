//######################################################--echart1_First_time_report--###########################################
function echart1(){

var dom1 = document.getElementById("echart_line1");
var myChart1 = echarts.init(dom1);
var app = {};
option1 = null;
option1 = {
    responsive: true,
    
    tooltip: {
        trigger: 'axis'
    },
    
    toolbox: {
        show: true,
        feature: {
            magicType: {show: true, type: ['stack', 'tiled','line','bar'],title: {
						line: 'Line',
						bar: 'Bar',
						stack: 'Stack',
						tiled: 'Tiled'
					  }},
            saveAsImage: {show: true}
        }
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
        data: [10,20,30,40,50,60,70,80,90,100,120,140,150,160,170,180,190,220],
        show:false
    },
    yAxis: {
        show:false
    },
    series: [{
        type: 'line',
        smooth: true,
        data: []
    },
    {
        
        type: 'line',
        smooth: true,
        data: [20,25,40,60,20,40]
    },
    {
        type: 'line',
        smooth: true,
        data: [10,15,9,40,20,70,-20]
    },{
				  name: 'Maximum',
				  type: 'line',
                  smooth: true,
                  color: 'rgba(0, 0, 0, 0.1)',  
				  itemStyle: {
					normal: {
					  areaStyle: {
                        type: 'default',
                        color: '#c9c7c7'
					  }
					}
				  },
				  data: [100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100]
				}]
};;
    var tem=0;
	var tem1 = 5;
	function xyz1(){
        if (option1 && typeof option1 === "object") {
    myChart1.setOption(option1, true);
	}
	
	var t = new Date().getMinutes();
	option1.series[3].data.push(100);
	option1.series[1].data.push(20);
	option1.xAxis.data.push(t);
	}
	 interval1 = setInterval(xyz1,3000);

}

//################################################--End_of_First_time_report_-----##################################################3


//######################################################--echart1_Down_time_reason--###########################################
function echart2(){

	var dom2 = document.getElementById("echart_line2");
	var myChart2 = echarts.init(dom2);
	var app = {};
	option2 = null;
	option2 = {
		responsive: true,
		
		tooltip: {
			trigger: 'axis'
		},
		
		toolbox: {
			show: true,
			feature: {
				magicType: {show: true, type: ['stack', 'tiled','line','bar'],title: {
							line: 'Line',
							bar: 'Bar',
							stack: 'Stack',
							tiled: 'Tiled'
						  }},
				saveAsImage: {show: true}
			}
		},
		xAxis: {
			type: 'category',
			boundaryGap: false,
			data: [10,20,30,40,50,60,70,80,90,100,120,140,150,160,170,180,190,220],
			show:false
		},
		yAxis: {
			show:false
		},
		series: [{
			type: 'line',
			smooth: true,
			data: []
		},
		{
			
			type: 'line',
			smooth: true,
			data: [20,25,40,60,20,40]
		},
		{
			type: 'line',
			smooth: true,
			data: [10,15,9,40,20,70,-20]
		},{
					  name: 'Maximum',
					  type: 'line',
					  smooth: true,
					  color: 'rgba(0, 0, 0, 0.1)',  
					  itemStyle: {
						normal: {
						  areaStyle: {
							type: 'default',
							color: '#c9c7c7'
						  }
						}
					  },
					  data: [100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100]
					}]
	};;
		var tem=0;
		var tem1 = 5;
		function xyz1(){
			if (option2 && typeof option2 === "object") {
		myChart2.setOption(option2, true);
		}
		
		var t = new Date().getMinutes();
		option2.series[3].data.push(100);
		option2.series[1].data.push(20);
		option2.xAxis.data.push(t);
		}
		 interval2 = setInterval(xyz1,3000);
	
	}
	
	//################################################--End_of_Down_time_reason-----##################################################3
	

	//######################################################--echart1_test_bed_utilization--###########################################
function echart3(){

	var dom3 = document.getElementById("echart_line3");
	var myChart3 = echarts.init(dom3);
	var app = {};
	option3 = null;
	option3 = {
		responsive: true,
		
		tooltip: {
			trigger: 'axis'
		},
		
		toolbox: {
			show: true,
			feature: {
				magicType: {show: true, type: ['stack', 'tiled','line','bar'],title: {
							line: 'Line',
							bar: 'Bar',
							stack: 'Stack',
							tiled: 'Tiled'
						  }},
				saveAsImage: {show: true}
			}
		},
		xAxis: {
			type: 'category',
			boundaryGap: false,
			data: [10,20,30,40,50,60,70,80,90,100,120,140,150,160,170,180,190,220],
			show:false
		},
		yAxis: {
			show:false
		},
		series: [{
			type: 'line',
			smooth: true,
			data: []
		},
		{
			
			type: 'line',
			smooth: true,
			data: [20,25,40,60,20,40]
		},
		{
			type: 'line',
			smooth: true,
			data: [10,15,9,40,20,70,-20]
		},{
					  name: 'Maximum',
					  type: 'line',
					  smooth: true,
					  color: 'rgba(0, 0, 0, 0.1)',  
					  itemStyle: {
						normal: {
						  areaStyle: {
							type: 'default',
							color: '#c9c7c7'
						  }
						}
					  },
					  data: [100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100]
					}]
	};;
		var tem=0;
		var tem1 = 5;
		function xyz3(){
			if (option3 && typeof option3 === "object") {
		myChart3.setOption(option3, true);
		}
		
		var t = new Date().getMinutes();
		option3.series[3].data.push(100);
		option3.series[1].data.push(20);
		option3.xAxis.data.push(t);
		}
		 interval3 = setInterval(xyz3,3000);
	
	}
	
	//################################################--End_of_test_bed_utilization-----##################################################3
	
//######################################################--echart4_test_bed_utilization_last--###########################################
function echart4(){

	var dom4 = document.getElementById("echart_line4");
	var myChart4 = echarts.init(dom4);
	var app = {};
	option4 = null;
	option4 = {
		responsive: true,
		
		tooltip: {
			trigger: 'axis'
		},
		
		toolbox: {
			show: true,
			feature: {
				magicType: {show: true, type: ['stack', 'tiled','line','bar'],title: {
							line: 'Line',
							bar: 'Bar',
							stack: 'Stack',
							tiled: 'Tiled'
						  }},
				saveAsImage: {show: true}
			}
		},
		xAxis: {
			type: 'category',
			boundaryGap: false,
			data: [10,20,30,40,50,60,70,80,90,100,120,140,150,160,170,180,190,220],
			show:false
		},
		yAxis: {
			show:false
		},
		series: [{
			type: 'line',
			smooth: true,
			data: []
		},
		{
			
			type: 'line',
			smooth: true,
			data: [20,25,40,60,20,40]
		},
		{
			type: 'line',
			smooth: true,
			data: [10,15,9,40,20,70,-20]
		},{
					  name: 'Maximum',
					  type: 'line',
					  smooth: true,
					  color: 'rgba(0, 0, 0, 0.1)',  
					  itemStyle: {
						normal: {
						  areaStyle: {
							type: 'default',
							color: '#c9c7c7'
						  }
						}
					  },
					  data: [100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100]
					}]
	};;
		var tem=0;
		var tem1 = 5;
		function xyz4(){
			if (option4 && typeof option4 === "object") {
		myChart4.setOption(option4, true);
		}
		
		var t = new Date().getMinutes();
		option4.series[3].data.push(100);
		option4.series[1].data.push(20);
		option4.xAxis.data.push(t);
		}
		 interval4 = setInterval(xyz4,3000);
	
	}
	
	//################################################--End_of_test_bed_capacity_last-----##################################################3
	

//######################################################--echart5_First_time_report--###########################################
function echart5(){

	dom5 = document.getElementById("echart_line");
		myChart5 = echarts.init(dom5);
	var app = {};
	option5 = null;
	option5 = {
		responsive: true,
		
		tooltip: {
			trigger: 'axis'
		},
		
		toolbox: {
			show: true,
			feature: {
				magicType: {show: true, type: ['stack', 'tiled','line','bar'],title: {
							line: 'Line',
							bar: 'Bar',
							stack: 'Stack',
							tiled: 'Tiled'
						  }},
				saveAsImage: {show: true}
			}
		},
		xAxis: {
			type: 'category',
			boundaryGap: false,
			data: [10,20,30,40,50,60,70,80,90,100,120,140,150,160,170,180,190,220],
			show:false
		},
		yAxis: {
			show:false
		},
		series: [{
			type: 'line',
			smooth: true,
			data: []
		},
		{
			
			type: 'line',
			smooth: true,
			data: [20,25,40,60,20,40]
		},
		{
			type: 'line',
			smooth: true,
			data: [10,15,9,40,20,70,-20]
		},{
					  name: 'Maximum',
					  type: 'line',
					  smooth: true,
					  color: 'rgba(0, 0, 0, 0.1)',  
					  itemStyle: {
						normal: {
						  areaStyle: {
							type: 'default',
							color: '#c9c7c7'
						  }
						}
					  },
					  data: [100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100]
					}]

	};;
		
		function xyz5(){
			clearInterval(xyz5,3000);
			if (option5 && typeof option5 === "object") {
		myChart5.setOption(option5, true);
		}
			var t = new Date().getMinutes();
			option5.series[3].data.push(100);
			option5.series[1].data.push(20);
			option5.xAxis.data.push(t);
		}
		 interval5 = setInterval(xyz5,3000);
	
	}
	
	//################################################--End_of_First_time_report_-----##################################################3
	



$(document).ready(function() {

	echart1();
	echart2();
	echart3();
	echart4();
	echart5();
			
});	