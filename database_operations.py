import pymongo
import pandas
import numpy as np
import pymongo
from datetime import datetime, timedelta
import pytz
from excel_operations import update_basic_details_auto,excel_to_pdf

# -----------------------KpclTestbed Database-------------------
myclient = pymongo.MongoClient("mongodb://localhost:27017/")
kpcl_testbed = myclient["KpclTestbed"]
test = kpcl_testbed["test"]

# ----------------Model_database-------------------
modal_db = myclient["model_database"]
modal_collection = modal_db["screw_compressor"]


def return_model_details(name, sheet, model, pressure, series):

    excel_data_df = pandas.read_excel(
        name, sheet_name=sheet, keep_default_na=False)
    '''print(excel_data_df.columns.ravel())
    print("---------------------------------")
    print(excel_data_df.Model.unique())
    print("---------------------------------")
    print(excel_data_df.Series.unique())
    print("---------------------------------")
    print(excel_data_df.Pressure.unique())'''

    dict_data = excel_data_df.to_dict(orient='records')
    # =============================================================================================
    print("======================================================================")

    myclient = pymongo.MongoClient("mongodb://localhost:27017/")
    mydb = myclient["model_database"]
    mycol = mydb["screw_compressor"]
    mycol.drop()
    mycol = mydb["screw_compressor"]
    x = mycol.insert_many(dict_data)

    myquery = {"$and": [{"Model": model}, {
        'Operating Pressure': pressure}, {'Series': series}]}
    mydoc = mycol.find(myquery, {"_id": 0})

    all_possibilites = {'item_count': 0, 'items': []}
    for x in mydoc:
        print(x)
        all_possibilites['items'].append(x)
        all_possibilites['item_count'] += 1
    print(all_possibilites)
    return all_possibilites


# ---------------------------created by anirudh----------------------

def kpcl_database(data):

    try:
        model = data["Software"]["Compressor Model"]
        pressure = data["Software"]["Compressor Pressure"]
        series = data["Software"]["Compressor Sr Mo"]

        try:
            mydoc = modal_collection.find_one({"Model": model, 'Operating Pressure': int(
                pressure), 'Series': series}, {"_id": 0})
            data["Software"]["Nozzle Diameter"] = mydoc["Nozzle Size"],
            data["Software"]["Safety Valve Pressure"] = mydoc["Safety Valve Pressure"],
            data["Software"]["Loading Pressure in Kg/cm2"] = mydoc["Loading Pressure"],
            data["Software"]["Unloading Pressure in Kg/cm2"] = mydoc["Unloading Pressure"],
            '''
            current_time = datetime.now(pytz.timezone('Asia/Kolkata'))
            date_obj = current_time.date()
            start_time = current_time.time()
            temp = current_time + timedelta(hours=3)
            stop_time = temp.time()

            data["Software"]["Test Date"] = date_obj
            data["Software"]["Test Start Time"] = start_time
            data["Software"]["Test Stop Time"] = stop_time

            t_reding_1 = (current_time + timedelta(minutes=45)).time()
            t_reding_2 = (current_time + timedelta(minutes=90)).time()
            t_reding_3 = (current_time + timedelta(minutes=135)).time()
            t_reding_4 = (current_time + timedelta(minutes=180)).time()

            data["Reading 1 Timestamp"] = t_reding_1
            data["Reading 2 Timestamp"] = t_reding_2
            data["Reading 3 Timestamp"] = t_reding_3
            data["Reading 4 Timestamp"] = t_reding_4
            '''
            test.insert_one(data)
            basic_details = test.find_one({},{"_id":0})
            update_basic_details_auto(basic_details,name="final_format.xlsx")
            excel_to_pdf()

        except:
            mydoc = modal_collection.find_one({"Model": model, 'Operating Pressure': float(
                pressure), 'Series': series}, {"_id": 0})
            data["Software"]["Nozzle Diameter"] = mydoc["Nozzle Size"],
            data["Software"]["Safety Valve Pressure"] = mydoc["Safety Valve Pressure"],
            data["Software"]["Loading Pressure in Kg/cm2"] = mydoc["Loading Pressure"],
            data["Software"]["Unloading Pressure in Kg/cm2"] = mydoc["Unloading Pressure"],

            '''
            current_time = datetime.now(pytz.timezone('Asia/Kolkata'))
            date_obj = current_time.date()
            start_time = current_time.time()
            temp = current_time + timedelta(hours=3)
            stop_time = temp.time()

            data["Software"]["Test Date"] = date_obj
            data["Software"]["Test Start Time"] = start_time
            data["Software"]["Test Stop Time"] = stop_time

            t_reding_1 = (current_time + timedelta(minutes=45)).time()
            t_reding_2 = (current_time + timedelta(minutes=90)).time()
            t_reding_3 = (current_time + timedelta(minutes=135)).time()
            t_reding_4 = (current_time + timedelta(minutes=180)).time()

            data["Reading 1 Timestamp"] = t_reding_1
            data["Reading 2 Timestamp"] = t_reding_2
            data["Reading 3 Timestamp"] = t_reding_3
            data["Reading 4 Timestamp"] = t_reding_4
            '''
            test.insert_one(data)
            basic_details = test.find_one({},{"_id":0})
            update_basic_details_auto(basic_details,name="final_format.xlsx")
            excel_to_pdf()

        print("+++++++++++++++++++ REturn +++++++++++++++++++++++++++++++++++++++++++")
        print(mydoc, pressure, model, series)
        return mydoc
    except:
        print(
            "+++++++++++++++++++ Error +++++++++++++++++++++++++++++++++++++++++++")
        return 0


def dummy():
    print(test.find_one({}, {"_id": 0}))


# -------------------------------------------------------------------

mydb1 = myclient['Anzen_1_0']
user_collection = mydb1['Users']

def verify_credentials(data):
    myquery = {"phone": str(data["phone"])}
    mydoc = user_collection.find(myquery, {'password': True, 'sign_up_status': True, "_id": False})
    credentials = []
    for x in mydoc:
        credentials.append(x)
    if len(credentials)>0:
        if data["password"] == credentials[0]["password"]:
            return "login successfully"
    else:
        return 1


def return_credentials(phone):
    myquery = {"phone": str(phone)}
    mydoc = user_collection.find(myquery, {'name': True, 'password': True, 'role': True, 'o_id': True, "_id": False})
    credentials = []
    for x in mydoc:
        credentials.append(x)
    if len(credentials) > 0:
        return credentials[0]
    else:
        return 0